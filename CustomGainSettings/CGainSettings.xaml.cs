﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static CustomGainSettings.ApplicationViewModel;

namespace CustomGainSettings
{
    /// <summary>
    /// Interaction logic for CGainSettings.xaml
    /// </summary>
    public partial class CGainSettings : UserControl
    {
        Settings _Settings;
        Model _Model;

        ApplicationViewModel _ApplicationViewModel;


        private Theme _CurrentTheme;
        public Theme CurrentTheme
        {
            get { return _CurrentTheme; }
            set
            {
                _CurrentTheme = value;
                SetTheme();
            }
        }

        public enum Theme
        {
            Orange,
            Blue
        }

        public string GainSettingsName { get; set; }

        public IEnumerable<CustomGroup> CustomGroups { get; set; }


        Dictionary<Slider, DestinationCustomSlider> SliderDestination = new Dictionary<Slider, DestinationCustomSlider>();

        public void SetValue(int GroupIndex, int RowIndex, double Value)
        {
            if (GroupIndex < CustomGroups.Count())
            {
                if (RowIndex < CustomGroups.ElementAt(GroupIndex).CustomSliders.Count())
                {
                    CustomGroups.ElementAt(GroupIndex).CustomSliders.ElementAt(RowIndex).CurrentValue = Value;
                }
            }
        }

        public CGainSettings()
        {
            InitializeComponent();

            _Model = new Model();
            _Settings = new Settings()
            {
                RangeXMin = 10,
                BandwidthMHz = 62.5,
                NumberOfBands = 96
            };

            _ApplicationViewModel = new ApplicationViewModel(_Model, _Settings);

            DataContext = _ApplicationViewModel;

            SetTheme();
        }

        private void SetTheme()
        {
            this.Resources.MergedDictionaries.Clear();

            ResourceDictionary src1 = new ResourceDictionary();
            ResourceDictionary src2 = new ResourceDictionary();
            ResourceDictionary src3 = new ResourceDictionary();

            if (_CurrentTheme == Theme.Orange)
            {
                src1.Source = new Uri("/CustomGainSettings;component/Theme/Core.xaml", UriKind.RelativeOrAbsolute);
                src2.Source = new Uri("/CustomGainSettings;component/Theme/Brushes.xaml", UriKind.RelativeOrAbsolute);
                src3.Source = new Uri("/CustomGainSettings;component/Theme/Icons.xaml", UriKind.RelativeOrAbsolute);
            }
            if (_CurrentTheme == Theme.Blue)
            {
                src1.Source = new Uri("/CustomGainSettings;component/Theme/CoreBlue.xaml", UriKind.RelativeOrAbsolute);
                src2.Source = new Uri("/CustomGainSettings;component/Theme/BrushesBlue.xaml", UriKind.RelativeOrAbsolute);
                src3.Source = new Uri("/CustomGainSettings;component/Theme/Icons.xaml", UriKind.RelativeOrAbsolute);
            }

            this.Resources.MergedDictionaries.Add(src1);
            this.Resources.MergedDictionaries.Add(src2);
            this.Resources.MergedDictionaries.Add(src3);

            _Model.ControlLightBackground = TryFindResource("ControlLightBackground") as LinearGradientBrush;
            _Model.ControlBorderBrush = TryFindResource("ControlBorderBrush") as LinearGradientBrush;
            _Model.ControlForegroundWhite = TryFindResource("ControlForegroundWhite") as SolidColorBrush ?? new SolidColorBrush(Colors.Black);
        }

        public CGainSettings(Settings Settings)
        {
            InitializeComponent();

            _Model = new Model();
            _Settings = Settings;

            _ApplicationViewModel = new ApplicationViewModel(_Model, _Settings);

            DataContext = _ApplicationViewModel;
        }

        public void Init()
        {
            Binding myBind = new Binding()
            {
                Source = this,
                Path = new PropertyPath("GainSettingsName"),
                Converter = this.Resources["FormatConverter"] as IValueConverter,
                Mode = BindingMode.TwoWay
            };
            MainGB.SetBinding(HeaderedContentControl.HeaderProperty, myBind);

            for (int i = 0; i < CustomGroups.Count(); i++)
            {
                MainSP.Children.Add(GenearateGroupBox(CustomGroups.ElementAt(i), i));
            }
        }

        private GroupBox GenearateGroupBox(CustomGroup customGroup, int index)
        {
            StackPanel stackPanelForGroupBox = new StackPanel();

            for (int i = 0; i < customGroup.CustomSliders.Count(); i++)
            {
                Slider tempSlider = GenetateSlider(customGroup.CustomSliders.ElementAt(i));

                SliderDestination.Add(tempSlider, new DestinationCustomSlider()
                {
                    GroupIndex = index,
                    RowIndex = i,
                    CustomSlider = customGroup.CustomSliders.ElementAt(i)
                });

                stackPanelForGroupBox.Children.Add(GenerateStackPanelForTextBlocks(tempSlider, customGroup, customGroup.CustomSliders.ElementAt(i), i));

                stackPanelForGroupBox.Children.Add(tempSlider);
            }

            GroupBox groupBox = GenerateGroupBox(index, CustomGroups.ElementAt(index).GroupName, stackPanelForGroupBox);

            return groupBox;
        }

        private GroupBox GenerateGroupBox(int index, string Name, object Content)
        {
            return new GroupBox()
            {
                Header = Name,
                Padding = new Thickness(5),
                Margin = (index == 0) ? new Thickness(0, 3, 0, 0) : new Thickness(0, 23, 0, 0),
                Content = Content
            };
        }

        private StackPanel GenerateStackPanelForTextBlocks(Slider slider, CustomGroup customGroup, CustomSlider customSlider, int index)
        {
            StackPanel stackPanelForTextBlocks = new StackPanel()
            {
                Orientation = Orientation.Horizontal,
                Margin = (index == 0) ? new Thickness(3) : new Thickness(3, 23, 3, 3)
            };

            Binding myBind = new Binding()
            {
                Source = customGroup,
                Path = new PropertyPath("SubGroupName"),
                Converter = this.Resources["FormatConverter"] as IValueConverter,
                Mode = BindingMode.TwoWay
            };
            TextBlock textBlock1 = new TextBlock()
            {
                Margin = new Thickness(3),
                Text = "",
            };
            textBlock1.SetBinding(TextBlock.TextProperty, myBind);

            Binding myBindExtra = new Binding()
            {
                Source = customSlider,
                Path = new PropertyPath("CurrentValue"),
                Converter = this.Resources["FormatConverter"] as IValueConverter,
                Mode = BindingMode.TwoWay
            };
            slider.SetBinding(Slider.ValueProperty, myBindExtra);

            Binding myBind2 = new Binding()
            {
                Source = slider,
                Path = new PropertyPath("Value"),
                Converter = this.Resources["FormatConverter"] as IValueConverter,
                Mode = BindingMode.TwoWay
            };
            TextBlock textBlock2 = new TextBlock()
            {
                Margin = new Thickness(3)
            };
            textBlock2.SetBinding(TextBlock.TextProperty, myBind2);

            Binding myBind3 = new Binding()
            {
                Source = _Model,
                Path = new PropertyPath("dB"),
                Converter = this.Resources["FormatConverter"] as IValueConverter,
                Mode = BindingMode.TwoWay
            };
            TextBlock textBlock3 = GenerateTextBlock();
            textBlock3.SetBinding(TextBlock.TextProperty, myBind3);



            stackPanelForTextBlocks.Children.Add(textBlock1);
            stackPanelForTextBlocks.Children.Add(GenerateTextBlock($"№{index + 1}"));
            stackPanelForTextBlocks.Children.Add(textBlock2);
            stackPanelForTextBlocks.Children.Add(textBlock3);

            return stackPanelForTextBlocks;

            TextBlock GenerateTextBlock(string Text = "")
            {
                return new TextBlock()
                {
                    Margin = new Thickness(3),
                    Text = Text
                };
            }
        }
        private Slider GenetateSlider(CustomSlider customSlider)
        {
            Slider slider = new Slider()
            {
                Minimum = customSlider.MinValue,
                Maximum = customSlider.MaxValue,
                Value = customSlider.CurrentValue,
                TickPlacement = System.Windows.Controls.Primitives.TickPlacement.BottomRight,
                SmallChange = customSlider.TickValue,
                LargeChange = customSlider.TickValue,
                AutoToolTipPlacement = System.Windows.Controls.Primitives.AutoToolTipPlacement.BottomRight,
                AutoToolTipPrecision = 0,
                IsSnapToTickEnabled = true
            };

            slider.LostMouseCapture += CGainSettings_LostMouseCapture;

            return slider;
        }

        void CGainSettings_LostMouseCapture(object sender, MouseEventArgs e)
        {
            //Slider SliderTemp = (Slider)sender;
            DestinationCustomSlider destinationCustomSliderTemp = SliderDestination[(Slider)sender];
            Console.WriteLine(destinationCustomSliderTemp);

            //SliderDestination[(Slider)sender].CustomSlider.SliderName += SliderTemp.Value;

            NotifyDestinationValue?.Invoke(new DestinationValue
            {
                GroupIndex = destinationCustomSliderTemp.GroupIndex,
                RowIndex = destinationCustomSliderTemp.RowIndex,
                Value = destinationCustomSliderTemp.CustomSlider.CurrentValue
            });
        }

        public delegate void NotifyDestinationValueEvent(DestinationValue destinationValue);
        public event NotifyDestinationValueEvent NotifyDestinationValue;



        private int _CurrentEPOValue = -1;
        private int CurrentEPOValue
        {
            get { return _CurrentEPOValue; }
            set
            {
                //_Settings.CurrentEPOValue = value;
                int newValue = value - 1;
                _CurrentEPOValue = newValue;
                if (CurrentEPOValue != -1)
                    Notify?.Invoke(Method.Get, newValue, 0, Devices.All);
            }
        }

        public delegate void NeededEvent(Method Method, int EPO, double Gain, Devices Devices);
        public event NeededEvent Notify;

        public enum Devices
        {
            Preselector1,
            Preselector2,
            Preselector3,
            Preselector4,
            Receiver,
            All
        }
        public enum Method
        {
            Get,
            Set
        }

        private void iUD_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            CurrentEPOValue = (int)e.NewValue;
        }

        public void ChangeLanguage(Languages CurrentLanguage)
        {
            _ApplicationViewModel.CurrentLanguage = CurrentLanguage;
        }
    }
}