﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace CustomGainSettings
{
    public class CustomSlider : INotifyPropertyChanged
    {
        public double MinValue { get; set; }

        private double _CurrentValue { get; set; }
        public double CurrentValue
        {
            get { return _CurrentValue; }
            set
            {
                _CurrentValue = value;
                OnPropertyChanged(nameof(CurrentValue));
            }
        }

        public double MaxValue { get; set; }
        public double TickValue { get; set; }

        
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}