﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace CustomGainSettings
{
    public class Model : INotifyPropertyChanged
    {
        private string _EPO = "ЕПО";
        public string EPO
        {
            get { return _EPO; }
            set
            {
                _EPO = value;
                OnPropertyChanged("EPO");
            }
        }

        private string _MHz = "МГц";
        public string MHz
        {
            get { return _MHz; }
            set
            {
                _MHz = value;
                OnPropertyChanged("MHz");
            }
        }

        private string _dB = "дБ";
        public string dB
        {
            get { return _dB; }
            set
            {
                _dB = value;
                OnPropertyChanged("dB");
            }
        }


        private LinearGradientBrush _ControlLightBackground;
        public LinearGradientBrush ControlLightBackground
        {
            get { return _ControlLightBackground; }
            set
            {
                _ControlLightBackground = value;
                OnPropertyChanged(nameof(ControlLightBackground));
            }
        }

        private LinearGradientBrush _ControlBorderBrush;
        public LinearGradientBrush ControlBorderBrush
        {
            get { return _ControlBorderBrush; }
            set
            {
                _ControlBorderBrush = value;
                OnPropertyChanged(nameof(ControlBorderBrush));
            }
        }

        private SolidColorBrush _ControlForegroundWhite;
        public SolidColorBrush ControlForegroundWhite
        {
            get { return _ControlForegroundWhite; }
            set
            {
                _ControlForegroundWhite = value;
                OnPropertyChanged(nameof(ControlForegroundWhite));
            }
        }


        public Model()
        {
            InitBrushes();
        }


        private void InitBrushes()
        {
            GradientStopCollection gradientStops = new GradientStopCollection
                {
                    new GradientStop(ColorFromString("#FF878585"), 0),
                     new GradientStop(ColorFromString("#FF525252"), 1),
                };
            ControlLightBackground = new LinearGradientBrush(gradientStops, new Point(0, 0), new Point(0, 1));

            GradientStopCollection gradientStops2 = new GradientStopCollection
                {
                    new GradientStop(ColorFromString("#FF686868"), 0.204),
                     new GradientStop(ColorFromString("#FF686868"), 0.864)
                };
            ControlBorderBrush = new LinearGradientBrush(gradientStops2, new Point(0, 0.5), new Point(1, 0.5));

            ControlForegroundWhite = new SolidColorBrush(ColorFromString("#FFEEEFFF"));
        }

        private Color ColorFromString(string sharpString)
        {
            return (Color)ColorConverter.ConvertFromString(sharpString);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}