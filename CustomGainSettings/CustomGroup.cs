﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace CustomGainSettings
{
    public class CustomGroup : INotifyPropertyChanged
    {
        private string _GroupName;
        public string GroupName
        {
            get { return _GroupName; }
            set
            {
                _GroupName = value;
                OnPropertyChanged(nameof(GroupName));
            }
        }

        private string _SubGroupName;
        public string SubGroupName
        {
            get { return _SubGroupName; }
            set
            {
                _SubGroupName = value;
                OnPropertyChanged(nameof(SubGroupName));
            }
        }


        public IEnumerable<CustomSlider> CustomSliders { get; set; }


        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}