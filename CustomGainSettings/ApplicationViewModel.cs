﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CustomGainSettings
{
    public class ApplicationViewModel : INotifyPropertyChanged
    {
        private Model _Model = new Model();
        public Model Model
        {
            get { return _Model; }
            set
            {
                _Model = value;
                OnPropertyChanged(nameof(Model));
            }
        }

        private Settings _Settings = new Settings();
        public Settings Settings
        {
            get { return _Settings; }
            set
            {
                _Settings = value;
                OnPropertyChanged(nameof(Settings));
            }
        }

        public ApplicationViewModel(Model Model, Settings Settings)
        {
            this.Model = Model;
            this.Settings = Settings;

            SetDefaultLanguage();
        }


        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "") => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));


        public enum Languages : byte
        {
            [Description("Русский")]
            RU,
            [Description("English")]
            EN
        }

        private Languages _CurrentLanguage = Languages.RU;
        public Languages CurrentLanguage
        {
            get
            {
                return _CurrentLanguage;
            }
            set
            {
                _CurrentLanguage = value;
                ReTranslate();
            }
        }

        private void SetDefaultLanguage()
        {
            foreach (Languages Language in Enum.GetValues(typeof(Languages)))
            {
                string currentLanguageString = GetCurrentLanguageString();
                if (currentLanguageString.Contains(Language.ToString().ToLower()))
                {
                    CurrentLanguage = Language;
                    return;
                }
            }
            string GetCurrentLanguageString()
            {
                CultureInfo ci = CultureInfo.InstalledUICulture;
                return ci.Name;

                void Culture()
                {
                    CultureInfo ci1 = CultureInfo.InstalledUICulture;
                    Console.WriteLine("Installed Language Info:{0}", ci1.Name);
                    CultureInfo ci2 = CultureInfo.CurrentUICulture;
                    Console.WriteLine("Current UI Language Info: {0}", ci2.Name);
                }
            }
        }

        private Dictionary<string, string> LoadDictionary(Languages languages)
        {
            var translation = Properties.Resources.Translation;
            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(translation);

            Dictionary<string, string> TranslateDictionary = new Dictionary<string, string>();
            // получим корневой элемент
            XmlElement xRoot = xDoc.DocumentElement;
            foreach (XmlNode x2Node in xRoot.ChildNodes)
            {
                if (x2Node.NodeType == XmlNodeType.Comment)
                    continue;

                // получаем атрибут ID
                if (x2Node.Attributes.Count > 0)
                {
                    XmlNode attr = x2Node.Attributes.GetNamedItem("ID");
                    if (attr != null)
                    {
                        foreach (XmlNode childnode in x2Node.ChildNodes)
                        {
                            // если узел - language
                            if (childnode.Name == languages.ToString().ToLower())
                            {
                                if (!TranslateDictionary.ContainsKey(attr.Value))
                                    TranslateDictionary.Add(attr.Value, childnode.InnerText);
                            }
                        }
                    }
                }
            }

            return TranslateDictionary;
        }

        private void ReTranslate()
        {
            Dictionary<string, string> TranslateDictionary = LoadDictionary(CurrentLanguage);

            TranslateInnerClassProperties(_Model, TranslateDictionary);
            TranslateInnerClassProperties(_Settings, TranslateDictionary);
        }

        private void TranslateInnerClassProperties(object obj, Dictionary<string, string> TranslateDictionary)
        {
            Type type = obj.GetType();
            var properties = type.GetProperties();
            foreach (var prop in properties)
            {
                if (prop.PropertyType == typeof(string))
                {
                    if (TranslateDictionary.ContainsKey(prop.Name))
                    {
                        prop.SetValue(obj, TranslateDictionary[prop.Name]);
                    }
                }
            }
        }

    }
}
