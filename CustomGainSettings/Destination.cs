﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomGainSettings
{
    public class Destination
    {
        public int GroupIndex { get; set; }
        public int RowIndex { get; set; }
        public override string ToString()
        {
            return $"GroupIndex: {GroupIndex} RowIndex: {RowIndex}";
        }
    }

    public class DestinationCustomSlider : Destination
    {
        public CustomSlider CustomSlider { get; set; }

        public override string ToString()
        {
            return $"GroupIndex: {GroupIndex} RowIndex: {RowIndex} Value: {CustomSlider.CurrentValue}";
        }
    }

    public class DestinationValue : Destination
    {
        public double Value { get; set; }

        public override string ToString()
        {
            return $"GroupIndex: {GroupIndex} RowIndex: {RowIndex} Value: {Value}";
        }
    }
}