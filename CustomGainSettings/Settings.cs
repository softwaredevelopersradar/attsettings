﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace CustomGainSettings
{
    public class Settings : INotifyPropertyChanged
    {
        private int _NumberOfBands = 96;
        public int NumberOfBands
        {
            get { return _NumberOfBands; }
            set
            {
                _NumberOfBands = value;
                OnPropertyChanged(nameof(NumberOfBands));
                ReToolTip2();
            }
        }

        private double _BandwidthMHz = 62.5;
        public double BandwidthMHz
        {
            get { return _BandwidthMHz; }
            set
            {
                _BandwidthMHz = value;
                OnPropertyChanged(nameof(BandwidthMHz));
                ReToolTip2();
            }
        }

        private double _RangeXMin = 10.0;
        public double RangeXMin
        {
            get { return _RangeXMin; }
            set
            {
                _RangeXMin = value;
                OnPropertyChanged(nameof(RangeXMin));
                ReToolTip2();
            }
        }

        private string _ToolTip = "";
        public string ToolTip
        {
            get { return _ToolTip; }
            set
            {
                _ToolTip = value;
                OnPropertyChanged(nameof(ToolTip));
            }
        }
        private void ReToolTip()
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < _NumberOfBands; i++)
            {
                sb.Append($"{i + 1}: {GetStringRange(i)} \n");
            }
            ToolTip = sb.ToString();
        }

        private string _ToolTip1 = "";
        public string ToolTip1
        {
            get { return _ToolTip1; }
            set
            {
                _ToolTip1 = value;
                OnPropertyChanged(nameof(ToolTip1));
            }
        }

        private string _ToolTip2 = "";
        public string ToolTip2
        {
            get { return _ToolTip2; }
            set
            {
                _ToolTip2 = value;
                OnPropertyChanged(nameof(ToolTip2));
            }
        }

        private string _ToolTip3 = "";
        public string ToolTip3
        {
            get { return _ToolTip3; }
            set
            {
                _ToolTip3 = value;
                OnPropertyChanged(nameof(ToolTip3));
            }
        }

        private void ReToolTip2()
        {
            ReToolTip();

            double someNumber = NumberOfBands / 3d;
            int itemInColumn = (int)Math.Round(someNumber, MidpointRounding.AwayFromZero);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < itemInColumn; i++)
            {
                sb.Append($"{i + 1}: {GetStringRange(i)}  \n");
            }
            ToolTip1 = sb.ToString();

            sb = new StringBuilder();
            for (int i = itemInColumn; i < itemInColumn * 2; i++)
            {
                sb.Append($"{i + 1}: {GetStringRange(i)}  \n");
            }
            ToolTip2 = sb.ToString();

            sb = new StringBuilder();
            for (int i = itemInColumn * 2; i < NumberOfBands; i++)
            {
                sb.Append($"{i + 1}: {GetStringRange(i)}  \n");
            }
            ToolTip3 = sb.ToString();
        }

        private int _CurrentEPOValue = 0;
        public int CurrentEPOValue
        {
            get { return _CurrentEPOValue; }
            set
            {
                _CurrentEPOValue = value;
                OnPropertyChanged(nameof(CurrentEPOValue));
                ReRange(CurrentEPOValue);
            }
        }

        private string _Range = "";
        public string Range
        {
            get { return _Range; }
            set
            {
                _Range = value;
                OnPropertyChanged(nameof(Range));
            }
        }

        private void ReRange(int EPO)
        {
            Range = EPO == 0 ? "" : GetStringRange(EPO - 1);
        }
        private string GetStringRange(int EPO)
        {
            return $"{ _RangeXMin + EPO * BandwidthMHz:F1}-{ _RangeXMin + (EPO + 1) * BandwidthMHz:F1}";
        }


        public Settings()
        {
            ReToolTip2();
        }


        public void Init(int NumberOfBands, double BandwidthMHz, double RangeXMin)
        {
            this.NumberOfBands = NumberOfBands;
            this.BandwidthMHz = BandwidthMHz;
            this.RangeXMin = RangeXMin;
        }


        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}