﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            gSettings.Notify += GSettings_Notify;
        }

        private void GSettings_Notify(GainSettingsCuirasse.GSettings.Method Method, int EPO, double Gain, GainSettingsCuirasse.GSettings.Devices Devices)
        {
            Console.WriteLine($"GSettings_Notify: Method-{Method} EPO-{EPO} Gain-{Gain} Devices-{Devices}");
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //gSettings.CurrentEPOValue = 0;
            gSettings.SetCurrentEPOValue(0);
        }

        private void Button1_Click(object sender, RoutedEventArgs e)
        {
            //gSettings.CurrentEPOValue = 1;
            gSettings.SetCurrentEPOValue(1);
        }

        private void Button2_Click(object sender, RoutedEventArgs e)
        {
            gSettings.ChangeLanguage(GainSettingsCuirasse.ApplicationViewModel.Languages.RU);

        }

        private void Button3_Click(object sender, RoutedEventArgs e)
        {
            gSettings.ChangeLanguage(GainSettingsCuirasse.ApplicationViewModel.Languages.EN);

        }
    }
}
