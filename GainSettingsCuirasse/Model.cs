﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace GainSettingsCuirasse
{
    public class Model : INotifyPropertyChanged
    {
        private string _GainTune;
        private string _EPO;

        private string _MHz;
        private string _dB;

        public Model()
        {
            this._GainTune = "Настройка аттенюаторов";
            this._EPO = "ЕПО";

            this._MHz = "МГц";
            this._dB = "дБ";
        }

        public string GainTune
        {
            get { return _GainTune; }
            set
            {
                _GainTune = value;
                OnPropertyChanged(nameof(GainTune));
            }
        }
        public string EPO
        {
            get { return _EPO; }
            set
            {
                _EPO = value;
                OnPropertyChanged("EPO");
            }
        }
        public string MHz
        {
            get { return _MHz; }
            set
            {
                _MHz = value;
                OnPropertyChanged("MHz");
            }
        }
        public string dB
        {
            get { return _dB; }
            set
            {
                _dB = value;
                OnPropertyChanged("dB");
            }
        }

        private string _PreSelGain = "Усиление преселектора";
        public string PreSelGain
        {
            get { return _PreSelGain; }
            set
            {
                _PreSelGain = value;
                OnPropertyChanged(nameof(PreSelGain));
            }
        }

        private string _PreSelsGain = "Усиление преселекторов";
        public string PreSelsGain
        {
            get { return _PreSelsGain; }
            set
            {
                _PreSelsGain = value;
                OnPropertyChanged(nameof(PreSelsGain));
            }
        }

        private string _RecGain = "Усиление приёмника";
        public string RecGain
        {
            get { return _RecGain; }
            set
            {
                _RecGain = value;
                OnPropertyChanged(nameof(RecGain));
            }
        }

        private double _gPreSelValue1 = 0;
        public double gPreSelValue1
        {
            get { return _gPreSelValue1; }
            set
            {
                _gPreSelValue1 = value;
                OnPropertyChanged(nameof(gPreSelValue1));
            }
        }

        private double _gPreSelValue2 = 0;
        public double gPreSelValue2
        {
            get { return _gPreSelValue2; }
            set
            {
                _gPreSelValue2 = value;
                OnPropertyChanged(nameof(gPreSelValue2));
            }
        }

        private double _gPreSelValue3 = 0;
        public double gPreSelValue3
        {
            get { return _gPreSelValue3; }
            set
            {
                _gPreSelValue3 = value;
                OnPropertyChanged(nameof(gPreSelValue3));
            }
        }

        private double _gPreSelValue4 = 0;
        public double gPreSelValue4
        {
            get { return _gPreSelValue4; }
            set
            {
                _gPreSelValue4 = value;
                OnPropertyChanged(nameof(gPreSelValue4));
            }
        }

        private double _gRecValue = 0;
        public double gRecValue
        {
            get { return _gRecValue; }
            set
            {
                _gRecValue = value;
                OnPropertyChanged(nameof(gRecValue));
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
