﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static GainSettingsCuirasse.ApplicationViewModel;

namespace GainSettingsCuirasse
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class GSettings : UserControl
    {
        Settings _Settings;
        Model _Model;

        ApplicationViewModel _ApplicationViewModel;
        public GSettings()
        {
            InitializeComponent();

            _Model = new Model();
            _Settings = new Settings()
            {
                RangeXMin = 10,
                BandwidthMHz = 125,
                NumberOfBands = 48
            };

            _ApplicationViewModel = new ApplicationViewModel(_Model, _Settings);

            DataContext = _ApplicationViewModel;
        }

        public GSettings(Settings Settings)
        {
            InitializeComponent();

            _Model = new Model();
            _Settings = Settings;
            
            _ApplicationViewModel = new ApplicationViewModel(_Model, _Settings);

            DataContext = _ApplicationViewModel;
        }

        public void ChangeLanguage(Languages CurrentLanguage)
        {
            _ApplicationViewModel.CurrentLanguage = CurrentLanguage;
        }

        private int _CurrentEPOValue = -1;
        private int CurrentEPOValue
        {
            get { return _CurrentEPOValue; }
            set
            {
                //_Settings.CurrentEPOValue = value;
                int newValue = value - 1;
                _CurrentEPOValue = newValue;
                if (CurrentEPOValue != -1)
                    Notify?.Invoke(Method.Get, newValue, 0, Devices.All);
            }
        }

        public void SetCurrentEPOValue(int EPO)
        {
            _Settings.CurrentEPOValue = EPO;
        }

        private void iUD_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            CurrentEPOValue = (int)e.NewValue;
        }

        private void Sl1_LostMouseCapture(object sender, MouseEventArgs e)
        {
            if (CurrentEPOValue != -1)
                Notify?.Invoke(Method.Set, CurrentEPOValue, _Model.gPreSelValue1, Devices.Preselector1);
        }

        private void Sl2_LostMouseCapture(object sender, MouseEventArgs e)
        {
            if (CurrentEPOValue != -1)
                Notify?.Invoke(Method.Set, CurrentEPOValue, _Model.gPreSelValue2, Devices.Preselector2);
        }

        private void Sl3_LostMouseCapture(object sender, MouseEventArgs e)
        {
            if (CurrentEPOValue != -1)
                Notify?.Invoke(Method.Set, CurrentEPOValue, _Model.gPreSelValue3, Devices.Preselector3);
        }

        private void Sl4_LostMouseCapture(object sender, MouseEventArgs e)
        {
            if (CurrentEPOValue != -1)
                Notify?.Invoke(Method.Set, CurrentEPOValue, _Model.gPreSelValue4, Devices.Preselector4);
        }

        private void SlR_LostMouseCapture(object sender, MouseEventArgs e)
        {
            if (CurrentEPOValue != -1)
                Notify?.Invoke(Method.Set, CurrentEPOValue, _Model.gRecValue, Devices.Receiver);
        }


        public delegate void NeededEvent(Method Method, int EPO, double Gain, Devices Devices);
        public event NeededEvent Notify;


        public enum Devices
        {
            Preselector1,
            Preselector2,
            Preselector3,
            Preselector4,
            Receiver,
            All
        }
        public enum Method
        {
            Get,
            Set
        }

        public void SetGainOnControl(byte Gain, Devices Devices)
        {
            switch (Devices)
            {
                case Devices.Preselector1:
                    _Model.gPreSelValue1 = Gain;
                    break;
                case Devices.Preselector2:
                    _Model.gPreSelValue2 = Gain;
                    break;
                case Devices.Preselector3:
                    _Model.gPreSelValue3 = Gain;
                    break;
                case Devices.Preselector4:
                    _Model.gPreSelValue4 = Gain;
                    break;
                case Devices.Receiver:
                    _Model.gRecValue = Gain;
                    break;
            }
        }
       
    }
}
