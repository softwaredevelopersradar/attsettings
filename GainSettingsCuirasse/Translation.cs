﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Xml;

namespace GainSettingsCuirasse
{
    public class Translations
    {
        private Languages _CurrentLanguage = Languages.EN;
        public Languages CurrentLanguage
        {
            get
            {
                return _CurrentLanguage;
            }
            set
            {
                if (_CurrentLanguage != value)
                {
                    _CurrentLanguage = value;
                    ReTranslateAllControls();
                }
            }
        }

        public enum Languages : byte
        {
            [Description("Русский")]
            RU,
            [Description("English")]
            EN
        }

        public Translations()
        { 
        
        }

        private Dictionary<string, string> LoadDictionary(Languages languages)
        {
            var translation = Properties.Resources.Translation;
            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(translation);

            Dictionary<string, string> TranslateDictionary = new Dictionary<string, string>();
            // получим корневой элемент
            XmlElement xRoot = xDoc.DocumentElement;
            foreach (XmlNode x2Node in xRoot.ChildNodes)
            {
                if (x2Node.NodeType == XmlNodeType.Comment)
                    continue;

                // получаем атрибут ID
                if (x2Node.Attributes.Count > 0)
                {
                    XmlNode attr = x2Node.Attributes.GetNamedItem("ID");
                    if (attr != null)
                    {
                        foreach (XmlNode childnode in x2Node.ChildNodes)
                        {
                            // если узел - language
                            if (childnode.Name == languages.ToString().ToLower())
                            {
                                if (!TranslateDictionary.ContainsKey(attr.Value))
                                    TranslateDictionary.Add(attr.Value, childnode.InnerText);
                            }
                        }
                    }
                }
            }

            return TranslateDictionary;
        }

        public void ReTranslateAllControls()
        {
            Dictionary<string, string> TranslateDictionary = LoadDictionary(CurrentLanguage);

            TranslateProperties(TranslateDictionary);
        }

        public void GetAllClasses(object obj)
        {
            Type ourtype = obj.GetType();

            var properties = ourtype.GetProperties();

            for (int i = 0; i < properties.Count(); i++)
            {
                Type Type = Type.GetType((properties[i].PropertyType.FullName));

                F3(properties[i], Type);
            }

            IEnumerable<Type> list = Assembly.GetAssembly(ourtype).GetTypes().Where(type => type.IsSubclassOf(ourtype));  // using System.Linq

            foreach (Type itm in list)
            {
                Console.WriteLine(itm);
            }
        }

        public void SetDefaultLanguage()
        {
            foreach (Languages Language in Enum.GetValues(typeof(Languages)))
            {
                //Console.WriteLine(Language);
                string currentLanguageString = GetCurrentLanguageString();
                if (currentLanguageString.Contains(Language.ToString().ToLower()))
                {
                    CurrentLanguage = Language;
                    return;
                }
            }
        }

        private string GetCurrentLanguageString()
        {
            CultureInfo ci = CultureInfo.InstalledUICulture;
            return ci.Name;

            void Culture()
            {
                CultureInfo ci1 = CultureInfo.InstalledUICulture;
                Console.WriteLine("Installed Language Info:{0}", ci1.Name);
                CultureInfo ci2 = CultureInfo.CurrentUICulture;
                Console.WriteLine("Current UI Language Info: {0}", ci2.Name);
            }
        }

        public void F(Model model)
        {
            Dictionary<string, string> TranslateDictionary = LoadDictionary(CurrentLanguage);

            Type type = model.GetType();
            var properties = type.GetProperties();
            foreach (var prop in properties)
            {
                if (prop.PropertyType == typeof(string))
                {
                    if (TranslateDictionary.ContainsKey(prop.Name))
                    {
                        prop.SetValue(model, TranslateDictionary[prop.Name]);
                    }
                }
            }
        }
        public void F2(object obj)
        {
            Dictionary<string, string> TranslateDictionary = LoadDictionary(CurrentLanguage);

            Type type = obj.GetType();

            var properties = type.GetProperties();
            foreach (var prop in properties)
            {
                if (prop.PropertyType == typeof(string))
                {
                    if (TranslateDictionary.ContainsKey(prop.Name))
                    {
                        prop.SetValue(obj, TranslateDictionary[prop.Name]);
                    }
                }
            }
        }
        void F3(object obj, Type type)
        {
            Dictionary<string, string> TranslateDictionary = LoadDictionary(CurrentLanguage);

            var properties = type.GetProperties();
            foreach (var prop in properties)
            {
                if (prop.PropertyType == typeof(string))
                {
                    if (TranslateDictionary.ContainsKey(prop.Name))
                    {
                        //Activator.CreateInstance(t);
                        prop.SetValue(obj, TranslateDictionary[prop.Name]);
                    }
                }
            }
        }

        private void TranslateProperties(Dictionary<string, string> TranslateDictionary)
        {
            var properties = typeof(Model).GetProperties();

            foreach (var prop in properties)
            {
                if (prop.PropertyType == typeof(string))
                {
                    if (TranslateDictionary.ContainsKey(prop.Name))
                    {
                        prop.SetValue(this, TranslateDictionary[prop.Name]);
                    }
                }
            }
        }


    }
}
