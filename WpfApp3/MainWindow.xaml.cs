﻿using CustomGainSettings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Init();
        }

        private void Init()
        {
            CustomSlider customSlider11 = new CustomSlider()
            {
                MinValue = 0,
                MaxValue = 30,
                CurrentValue = 10,
                TickValue = 1
            };
            CustomSlider customSlider12 = new CustomSlider()
            {
                MinValue = 0,
                MaxValue = 30,
                CurrentValue = 20,
                TickValue = 1
            };
            CustomSlider customSlider13 = new CustomSlider()
            {
                MinValue = 0,
                MaxValue = 30,
                CurrentValue = 30,
                TickValue = 1
            };

            CustomSlider customSlider21 = new CustomSlider()
            {
                MinValue = 0,
                MaxValue = 30,
                CurrentValue = 21,
                TickValue = 1
            };
            CustomSlider customSlider22 = new CustomSlider()
            {
                MinValue = 0,
                MaxValue = 30,
                CurrentValue = 12,
                TickValue = 1
            };

            CustomGroup customGroup1 = new CustomGroup()
            {
                GroupName = "Усиление преселекторов",
                SubGroupName = "Усиление преселектора",
                CustomSliders = new CustomSlider [] { customSlider11, customSlider12, customSlider13 }
            };
            CustomGroup customGroup2 = new CustomGroup()
            {
                GroupName = "Усиление приёмников",
                SubGroupName = "Усиление приёмника",
                CustomSliders = new CustomSlider[] { customSlider21, customSlider22}
            };

            cGainSettings.GainSettingsName = "Контроль усиления";
            cGainSettings.CustomGroups = new CustomGroup[] { customGroup1, customGroup2 };

            cGainSettings.Init();

            cGainSettings.SetValue(1, 1, 3);
            cGainSettings.SetValue(0, 2, 5);

            //cGainSettings.ChangeLanguage(ApplicationViewModel.Languages.EN);
        }
    }
}
