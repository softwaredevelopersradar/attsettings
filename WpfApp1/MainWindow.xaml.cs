﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            attSet.AttValueChange += AttSet_AttValueChange;
        }

        private void AttSet_AttValueChange(object sender, int BandBumber, double AttValue)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            attSet.Band = 1;
            attSet.Att = 13.5;
            attSet.Amp = 114.5;
        }

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);

            // Begin dragging the window
            this.DragMove();
        }

        private void Button1_Click(object sender, RoutedEventArgs e)
        {
            attSet.Band = 174;
            attSet.Att = 13.5;
            attSet.Amp = 114.5;
        }

        private void Button2_Click(object sender, RoutedEventArgs e)
        {
            attSet.SetLanguage("rus");
        }

        private void Button3_Click(object sender, RoutedEventArgs e)
        {
            attSet.SetLanguage("eng");
        }

        private void Button4_Click(object sender, RoutedEventArgs e)
        {
            attSet.SetLanguage("az");
        }
    }
}
