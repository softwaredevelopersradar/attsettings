﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace AttSettings
{
    class Translations : INotifyPropertyChanged
    {

        private string _AttTune;
        private string _EPO;

        private string _Att;
        private string _Amp;
        private string _Total;

        private string _MHz;
        private string _dB;

        public Translations()
        {
            this._AttTune = "Настройка аттенюаторов";
            this._EPO = "ЕПО";
            this._Att = "Аттенюатор:";
            this._Amp = "Усилитель:";
            this._Total = "Всего:";

            this._MHz = "МГц";
            this._dB = "дБ";
        }

        public Translations(Translations translations)
        {
            this._AttTune = translations._AttTune;
            this._EPO = translations._EPO;
            this._Att = translations._Att;
            this._Amp = translations._Amp;
            this._Total = translations._Total;

            this._MHz = translations._MHz;
            this._dB = translations._dB;
        }

        public string AttTune
        {
            get { return _AttTune; }
            set
            {
                _AttTune = value;
                OnPropertyChanged("AttTune");
            }
        }
        public string EPO
        {
            get { return _EPO; }
            set
            {
                _EPO = value;
                OnPropertyChanged("EPO");
            }
        }
        public string Att
        {
            get { return _Att; }
            set
            {
                _Att = value;
                OnPropertyChanged("Att");
            }
        }
        public string Amp
        {
            get { return _Amp; }
            set
            {
                _Amp = value;
                OnPropertyChanged("Amp");
            }
        }
        public string Total
        {
            get { return _Total; }
            set
            {
                _Total = value;
                OnPropertyChanged("Total");
            }
        }
        public string MHz
        {
            get { return _MHz; }
            set
            {
                _MHz = value;
                OnPropertyChanged("MHz");
            }
        }
        public string dB
        {
            get { return _dB; }
            set
            {
                _dB = value;
                OnPropertyChanged("dB");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
