﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AttSettings
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class AttSet : UserControl
    {
        public AttSet()
        {
            InitializeComponent();
            Init();
        }

        private void Init()
        {
            Band = -1;
            Att = 0;
            Amp = 0;
            NeedInit?.Invoke(this);
        }

        
        string sAtt = "Аттенюатор:";
        string sAmp = "Усилитель:";
        string sTotal = "Всего:";
        string sMHz = "МГц";
        string sdB = "дБ";

        public void SetLanguage(string param)
        {
            if (param.ToLower().Contains("rus"))
            {
                var trans = (Translations)this.Resources["translate"];
                trans.AttTune = "Настройка аттенюаторов";
                trans.EPO = "ЕПО";

                sAtt = "Аттенюатор:";
                sAmp = "Усилитель:";
                sTotal = "Всего:";
                sMHz = "МГц";
                sdB = "дБ";

                UpdateAllLabels();
                return;
            }
            if (param.ToLower().Contains("eng"))
            {
                var trans = (Translations)this.Resources["translate"];
                trans.AttTune = "Attenuators tuning";
                trans.EPO = "Single span";

                sAtt = "Attenuator:";
                sAmp = "Amplifier:";
                sTotal = "Total:";
                sMHz = "MHz";
                sdB = "dB";

                UpdateAllLabels();
                return;
            }
            if (param.ToLower().Contains("az"))
            {
                var trans = (Translations)this.Resources["translate"];
                trans.AttTune = "Atteniyuator tənzimləmə";
                trans.EPO = "BBZ";

                sAtt = "Atteniyuator:";
                sAmp = "Gücləndirici:";
                sTotal = "Cəmi:";
                sMHz = "MHs";
                sdB = "dB";

                UpdateAllLabels();
                return;
            }
        }

        private void UpdateAllLabels()
        {
            tbRange.Text = Range(_Band);

            tbAtt.Text = sAtt + " " + _Att.ToString("F1") + " " + sdB;
            tbAmp.Text = sAmp + " " + _Amp.ToString("F1") + " " + sdB;
            tbRes.Text = sTotal + " " + _Res.ToString("F1") + " " + sdB;
        }

        public delegate void AttValueChangeEvent(object sender, int BandBumber, double AttValue);
        public event AttValueChangeEvent AttValueChange;

        public delegate void Event(object sender);
        public event Event NeedInit;
        public event Event NeedClose;

        public delegate void NeedGetRequestEvent(object sender, int BandBumber);
        public event NeedGetRequestEvent NeedGetRequest;


        private int _BandCount = 200;
        public int BandCount
        {
            get { return _BandCount; }
            set
            {
                _BandCount = value;
                iUD.Maximum = _BandCount;
            }
        }

        private int _Band;
        public int Band
        {
            get { return _Band; }
            set
            {
                //if (_Band != value)
                {
                    if (value <= -1)
                    {
                        iUD.Value = 0;
                        if (tbRange != null)
                            tbRange.Text = "";
                    }
                    else
                    {
                        if (value < 100)
                        {
                            sl.Maximum = 40;
                        }
                        else
                        {
                            sl.Maximum = 70;
                        }

                        _Band = value;
                        iUD.Value = value + 1;
                        tbRange.Text = Range(_Band);
                    }
                }
            }
        }

        private string Range(int Band)
        {
            string s = "";
            string s1 = (25 + 30 * Band).ToString();
            string s2 = (55 + 30 * Band).ToString();
            s = s1 + "-" + s2 + " " + sMHz;
            return s;
        }

        private double _Att = 0;
        public double Att
        {
            get { return _Att; }
            set
            {
                _Att = (-1) * value;
                if (sl.Value != (-1) * _Att)
                    sl.Value = (-1) * _Att;
                tbAtt.Text = sAtt + " " + _Att.ToString("F1") + " " + sdB;
                Update();
            }
        }

        private double _Amp = 0;
        public double Amp
        {
            get { return _Amp; }
            set
            {
                _Amp = value;
                tbAmp.Text =  sAmp +" " + _Amp.ToString("F1") + " " + sdB;
                Update();
            }
        }

        private double _Res = 0;
        private double Res
        {
            get { return _Res; }
            set
            {
                _Res = value;
                tbRes.Text = sTotal + " " + _Res.ToString("F1") + " " + sdB;
            }
        }

        private void Update()
        {
            Res = _Att + _Amp;
        }

        private void Sl_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (Att != (-1) * sl.Value)
                Att = sl.Value;
        }

        private void Sl_LostMouseCapture(object sender, MouseEventArgs e)
        {
            AttValueChange?.Invoke(this, _Band, sl.Value);
        }

        private void bClose_Click(object sender, RoutedEventArgs e)
        {
            NeedClose?.Invoke(this);
        }

        private void iUD_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            try
            {
                Band = iUD.Value.Value - 1;
                NeedGetRequest?.Invoke(this, _Band);
            }
            catch { }
        }
    }
}
